-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Anamakine: 127.0.0.1
-- Üretim Zamanı: 07 Mar 2021, 02:00:20
-- Sunucu sürümü: 10.4.13-MariaDB
-- PHP Sürümü: 7.4.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Veritabanı: `liveware_blog_app`
--

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `categories`
--

CREATE TABLE `categories` (
  `category_id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Tablo döküm verisi `categories`
--

INSERT INTO `categories` (`category_id`, `title`, `slug`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Güncel Yaşam', 'guncel-yasam', NULL, NULL, NULL),
(2, 'Doğa', 'doga', NULL, NULL, NULL),
(3, 'Yazılım', 'yazilim', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Tablo döküm verisi `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2021_03_06_192121_posts', 1),
(5, '2021_03_06_192321_category', 1);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `posts`
--

CREATE TABLE `posts` (
  `post_id` bigint(20) UNSIGNED NOT NULL,
  `title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `vote` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Tablo döküm verisi `posts`
--

INSERT INTO `posts` (`post_id`, `title`, `slug`, `content`, `image`, `vote`, `user_id`, `category_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(2, 'Bu benim ilk blog içeriği', 'bu-benim-ilk-blog-icerigi', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur vitae mauris nibh. Quisque molestie ex orci, ac ultricies lorem sagittis ac. Nunc nec molestie felis. Vivamus ac cursus tortor. Sed ut imperdiet metus, eu vulputate eros. Quisque purus ex, cursus id mi eget, vehicula laoreet ante. Morbi maximus metus vitae erat vestibulum, quis eleifend tortor posuere. Donec finibus magna tortor, quis congue turpis malesuada sed. Duis a commodo tortor. Morbi condimentum interdum bibendum. Etiam varius urna eget enim viverra ornare. Pellentesque eu lacus eu dui ullamcorper tincidunt at id metus. Fusce pretium sapien et augue mattis vestibulum. Donec venenatis placerat ante. Aenean semper dignissim pharetra. Vestibulum porttitor commodo ante, vitae egestas nisi interdum nec.', 'bu-benim-ilk-blog-icerigi-1615064545.jpg', 0, 1, 2, '2021-03-06 18:02:25', '2021-03-06 18:02:25', NULL),
(3, 'Yeni bir doğa içeriği giriyorum', 'yeni-bir-doga-icerigi-giriyorum', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur vitae mauris nibh. Quisque molestie ex orci, ac ultricies lorem sagittis ac. Nunc nec molestie felis. Vivamus ac cursus tortor. Sed ut imperdiet metus, eu vulputate eros. Quisque purus ex, cursus id mi eget, vehicula laoreet ante. Morbi maximus metus vitae erat vestibulum, quis eleifend tortor posuere. Donec finibus magna tortor, quis congue turpis malesuada sed. Duis a commodo tortor. Morbi condimentum interdum bibendum. Etiam varius urna eget enim viverra ornare. Pellentesque eu lacus eu dui ullamcorper tincidunt at id metus. Fusce pretium sapien et augue mattis vestibulum. Donec venenatis placerat ante. Aenean semper dignissim pharetra. Vestibulum porttitor commodo ante, vitae egestas nisi interdum nec.\n\nIn elementum lacinia ipsum vitae pulvinar. Sed ac augue dictum, pulvinar mi et, finibus risus. Vivamus urna arcu, euismod vitae turpis vitae, fringilla consequat metus. Cras placerat pretium maximus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Phasellus cursus, tellus non ornare eleifend, nisi nisi finibus enim, nec tincidunt risus nibh at massa. Vestibulum egestas magna ut nibh vestibulum, congue viverra ex condimentum. Morbi bibendum lorem id massa dignissim, a molestie enim blandit.', 'yeni-bir-doga-icerigi-giriyorum-1615064605.jpg', 0, 1, 2, '2021-03-06 18:03:25', '2021-03-06 18:03:25', NULL),
(4, 'Bu kez güzel bir içerik gireceği', 'bu-kez-guzel-bir-icerik-girecegi', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur vitae mauris nibh. Quisque molestie ex orci, ac ultricies lorem sagittis ac. Nunc nec molestie felis. Vivamus ac cursus tortor. Sed ut imperdiet metus, eu vulputate eros. Quisque purus ex, cursus id mi eget, vehicula laoreet ante. Morbi maximus metus vitae erat vestibulum, quis eleifend tortor posuere. Donec finibus magna tortor, quis congue turpis malesuada sed. Duis a commodo tortor. Morbi condimentum interdum bibendum. Etiam varius urna eget enim viverra ornare. Pellentesque eu lacus eu dui ullamcorper tincidunt at id metus. Fusce pretium sapien et augue mattis vestibulum. Donec venenatis placerat ante. Aenean semper dignissim pharetra. Vestibulum porttitor commodo ante, vitae egestas nisi interdum nec.\n\nIn elementum lacinia ipsum vitae pulvinar. Sed ac augue dictum, pulvinar mi et, finibus risus. Vivamus urna arcu, euismod vitae turpis vitae, fringilla consequat metus. Cras placerat pretium maximus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Phasellus cursus, tellus non ornare eleifend, nisi nisi finibus enim, nec tincidunt risus nibh at massa. Vestibulum egestas magna ut nibh vestibulum, congue viverra ex condimentum. Morbi bibendum lorem id massa dignissim, a molestie enim blandit.\n\nDonec elementum eget lectus ac maximus. Sed sit amet leo lectus. Quisque vitae leo tristique, fermentum ligula a, consequat risus. In congue nisi id iaculis blandit. Suspendisse suscipit dui felis, in pharetra arcu interdum nec. Sed consequat vehicula tortor eget euismod. Sed commodo sapien nisi, ut ultricies arcu varius id. Vestibulum tempor diam in lectus ultricies, non faucibus elit aliquam.', 'bu-kez-guzel-bir-icerik-girecegi-1615064687.jpg', 0, 1, 1, '2021-03-06 18:04:47', '2021-03-06 18:04:47', NULL),
(5, 'Liveware Test', 'liveware-test', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur vitae mauris nibh. Quisque molestie ex orci, ac ultricies lorem sagittis ac. Nunc nec molestie felis. Vivamus ac cursus tortor. Sed ut imperdiet metus, eu vulputate eros. Quisque purus ex, cursus id mi eget, vehicula laoreet ante. Morbi maximus metus vitae erat vestibulum, quis eleifend tortor posuere. Donec finibus magna tortor, quis congue turpis malesuada sed. Duis a commodo tortor. Morbi condimentum interdum bibendum. Etiam varius urna eget enim viverra ornare. Pellentesque eu lacus eu dui ullamcorper tincidunt at id metus. Fusce pretium sapien et augue mattis vestibulum. Donec venenatis placerat ante. Aenean semper dignissim pharetra. Vestibulum porttitor commodo ante, vitae egestas nisi interdum nec.\n\nIn elementum lacinia ipsum vitae pulvinar. Sed ac augue dictum, pulvinar mi et, finibus risus. Vivamus urna arcu, euismod vitae turpis vitae, fringilla consequat metus. Cras placerat pretium maximus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Phasellus cursus, tellus non ornare eleifend, nisi nisi finibus enim, nec tincidunt risus nibh at massa. Vestibulum egestas magna ut nibh vestibulum, congue viverra ex condimentum. Morbi bibendum lorem id massa dignissim, a molestie enim blandit.\n\nDonec elementum eget lectus ac maximus. Sed sit amet leo lectus. Quisque vitae leo tristique, fermentum ligula a, consequat risus. In congue nisi id iaculis blandit. Suspendisse suscipit dui felis, in pharetra arcu interdum nec. Sed consequat vehicula tortor eget euismod. Sed commodo sapien nisi, ut ultricies arcu varius id. Vestibulum tempor diam in lectus ultricies, non faucibus elit aliquam.', 'liveware-test-1615065836.jpg', 0, 1, 2, '2021-03-06 18:23:56', '2021-03-06 18:23:56', NULL),
(6, 'Public path test', 'public-path-test', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur vitae mauris nibh. Quisque molestie ex orci, ac ultricies lorem sagittis ac. Nunc nec molestie felis. Vivamus ac cursus tortor. Sed ut imperdiet metus, eu vulputate eros. Quisque purus ex, cursus id mi eget, vehicula laoreet ante. Morbi maximus metus vitae erat vestibulum, quis eleifend tortor posuere. Donec finibus magna tortor, quis congue turpis malesuada sed. Duis a commodo tortor. Morbi condimentum interdum bibendum. Etiam varius urna eget enim viverra ornare. Pellentesque eu lacus eu dui ullamcorper tincidunt at id metus. Fusce pretium sapien et augue mattis vestibulum. Donec venenatis placerat ante. Aenean semper dignissim pharetra. Vestibulum porttitor commodo ante, vitae egestas nisi interdum nec.\n\n', 'public-path-test-1615065952.jpg', 0, 1, 1, '2021-03-06 18:25:52', '2021-03-06 18:25:52', NULL),
(7, 'Uzun bir blog yazısı', 'uzun-bir-blog-yazisi', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur vitae mauris nibh. Quisque molestie ex orci, ac ultricies lorem sagittis ac. Nunc nec molestie felis. Vivamus ac cursus tortor. Sed ut imperdiet metus, eu vulputate eros. Quisque purus ex, cursus id mi eget, vehicula laoreet ante. Morbi maximus metus vitae erat vestibulum, quis eleifend tortor posuere. Donec finibus magna tortor, quis congue turpis malesuada sed. Duis a commodo tortor. Morbi condimentum interdum bibendum. Etiam varius urna eget enim viverra ornare. Pellentesque eu lacus eu dui ullamcorper tincidunt at id metus. Fusce pretium sapien et augue mattis vestibulum. Donec venenatis placerat ante. Aenean semper dignissim pharetra. Vestibulum porttitor commodo ante, vitae egestas nisi interdum nec.\n\nIn elementum lacinia ipsum vitae pulvinar. Sed ac augue dictum, pulvinar mi et, finibus risus. Vivamus urna arcu, euismod vitae turpis vitae, fringilla consequat metus. Cras placerat pretium maximus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Phasellus cursus, tellus non ornare eleifend, nisi nisi finibus enim, nec tincidunt risus nibh at massa. Vestibulum egestas magna ut nibh vestibulum, congue viverra ex condimentum. Morbi bibendum lorem id massa dignissim, a molestie enim blandit.\n\nDonec elementum eget lectus ac maximus. Sed sit amet leo lectus. Quisque vitae leo tristique, fermentum ligula a, consequat risus. In congue nisi id iaculis blandit. Suspendisse suscipit dui felis, in pharetra arcu interdum nec. Sed consequat vehicula tortor eget euismod. Sed commodo sapien nisi, ut ultricies arcu varius id. Vestibulum tempor diam in lectus ultricies, non faucibus elit aliquam.\n\nUt ut ultrices nulla. Nam a ligula ut risus tincidunt laoreet et sed quam. Pellentesque quis convallis tortor, sit amet venenatis magna. Aliquam ut elementum sapien. In quis mattis risus. Ut ipsum sem, semper sed placerat eget, mattis vel ante. Fusce neque enim, pharetra id imperdiet vitae, ullamcorper id massa. Suspendisse potenti. Pellentesque eget leo suscipit metus scelerisque tincidunt.\n\nFusce eu eros sapien. Nullam auctor dui suscipit, semper leo sed, accumsan nunc. Quisque malesuada faucibus ex, sit amet porta ex maximus a. Donec vulputate malesuada purus, a mollis lacus congue a. Etiam et tempor velit. Etiam hendrerit, nunc sit amet ornare sodales, elit erat dignissim risus, vitae pellentesque arcu velit vitae tellus. Morbi cursus malesuada molestie. Nulla varius magna et mattis pellentesque. Integer elementum euismod sem et euismod. Duis sit amet diam nec mauris hendrerit tristique. Suspendisse potenti. Vestibulum rutrum enim vel egestas sollicitudin. Maecenas elementum eget dolor nec condimentum. Nunc sed mi pulvinar augue consequat dictum dapibus quis justo. Cras vitae nisi tristique, blandit mauris et, fringilla nisi.\n\n', 'uzun-bir-blog-yazisi-1615066023.jpg', 0, 1, 1, '2021-03-06 18:27:03', '2021-03-06 18:27:03', NULL),
(8, 'Resetting Pagination After Filtering Data', 'resetting-pagination-after-filtering-data', 'A common pattern when filtering a paginated result set is to reset the current page to \"1\" when filtering is applied.\n\nFor example, if a user visits page \"4\" of your data set, then types into a search field to narrow the results, it is usually desireable to reset the page to \"1\".\n\nLivewire\'s WithPagination trait exposes a ->resetPage() method to accomplish this.\n\nThis method can be used in combination with the updating/updated lifecycle hooks to reset the page when certain component data is updated.', 'resetting-pagination-after-filtering-data-1615066327.jpg', 0, 1, 3, '2021-03-06 18:32:07', '2021-03-06 18:32:07', NULL),
(9, 'reset page test', 'reset-page-test', 'A common pattern when filtering a paginated result set is to reset the current page to \"1\" when filtering is applied.\n\nFor example, if a user visits page \"4\" of your data set, then types into a search field to narrow the results, it is usually desireable to reset the page to \"1\".\n\nLivewire\'s WithPagination trait exposes a ->resetPage() method to accomplish this.\n\nThis method can be used in combination with the updating/updated lifecycle hooks to reset the page when certain component data is updated.', 'reset-page-test-1615067728.jpg', 0, 1, 3, '2021-03-06 18:55:28', '2021-03-06 18:55:28', NULL),
(10, 'loginden sonra yapılan yeni içerik', 'loginden-sonra-yapilan-yeni-icerik', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur vitae mauris nibh. Quisque molestie ex orci, ac ultricies lorem sagittis ac. Nunc nec molestie felis. Vivamus ac cursus tortor. Sed ut imperdiet metus, eu vulputate eros. Quisque purus ex, cursus id mi eget, vehicula laoreet ante. Morbi maximus metus vitae erat vestibulum, quis eleifend tortor posuere. Donec finibus magna tortor, quis congue turpis malesuada sed. Duis a commodo tortor. Morbi condimentum interdum bibendum. Etiam varius urna eget enim viverra ornare. Pellentesque eu lacus eu dui ullamcorper tincidunt at id metus. Fusce pretium sapien et augue mattis vestibulum. Donec venenatis placerat ante. Aenean semper dignissim pharetra. Vestibulum porttitor commodo ante, vitae egestas nisi interdum nec.\n\nIn elementum lacinia ipsum vitae pulvinar. Sed ac augue dictum, pulvinar mi et, finibus risus. Vivamus urna arcu, euismod vitae turpis vitae, fringilla consequat metus. Cras placerat pretium maximus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Phasellus cursus, tellus non ornare eleifend, nisi nisi finibus enim, nec tincidunt risus nibh at massa. Vestibulum egestas magna ut nibh vestibulum, congue viverra ex condimentum. Morbi bibendum lorem id massa dignissim, a molestie enim blandit.\n\nDonec elementum eget lectus ac maximus. Sed sit amet leo lectus. Quisque vitae leo tristique, fermentum ligula a, consequat risus. In congue nisi id iaculis blandit. Suspendisse suscipit dui felis, in pharetra arcu interdum nec. Sed consequat vehicula tortor eget euismod. Sed commodo sapien nisi, ut ultricies arcu varius id. Vestibulum tempor diam in lectus ultricies, non faucibus elit aliquam.\n\nUt ut ultrices nulla. Nam a ligula ut risus tincidunt laoreet et sed quam. Pellentesque quis convallis tortor, sit amet venenatis magna. Aliquam ut elementum sapien. In quis mattis risus. Ut ipsum sem, semper sed placerat eget, mattis vel ante. Fusce neque enim, pharetra id imperdiet vitae, ullamcorper id massa. Suspendisse potenti. Pellentesque eget leo suscipit metus scelerisque tincidunt.\n\nFusce eu eros sapien. Nullam auctor dui suscipit, semper leo sed, accumsan nunc. Quisque malesuada faucibus ex, sit amet porta ex maximus a. Donec vulputate malesuada purus, a mollis lacus congue a. Etiam et tempor velit. Etiam hendrerit, nunc sit amet ornare sodales, elit erat dignissim risus, vitae pellentesque arcu velit vitae tellus. Morbi cursus malesuada molestie. Nulla varius magna et mattis pellentesque. Integer elementum euismod sem et euismod. Duis sit amet diam nec mauris hendrerit tristique. Suspendisse potenti. Vestibulum rutrum enim vel egestas sollicitudin. Maecenas elementum eget dolor nec condimentum. Nunc sed mi pulvinar augue consequat dictum dapibus quis justo. Cras vitae nisi tristique, blandit mauris et, fringilla nisi.\n\n', '0', 0, 6, 1, '2021-03-06 21:29:33', '2021-03-06 21:29:33', NULL);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Tablo döküm verisi `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'fatih', 'fatih@test.com', NULL, 'eTYxdzCUvxaPdGMFaUz9TSJkRiSxJv7a', NULL, NULL, NULL, NULL),
(6, 'fatih_calisan', 'fatih@calisan.com', NULL, '$2y$10$1d1sFkzzzvEyDaqK2GIHRemILSjD0qF/LomjfC21kMffbbnbvDV86', NULL, '2021-03-06 21:22:15', '2021-03-06 21:22:15', NULL);

--
-- Dökümü yapılmış tablolar için indeksler
--

--
-- Tablo için indeksler `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`category_id`);

--
-- Tablo için indeksler `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Tablo için indeksler `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Tablo için indeksler `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`post_id`);

--
-- Tablo için indeksler `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Dökümü yapılmış tablolar için AUTO_INCREMENT değeri
--

--
-- Tablo için AUTO_INCREMENT değeri `categories`
--
ALTER TABLE `categories`
  MODIFY `category_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Tablo için AUTO_INCREMENT değeri `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Tablo için AUTO_INCREMENT değeri `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Tablo için AUTO_INCREMENT değeri `posts`
--
ALTER TABLE `posts`
  MODIFY `post_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- Tablo için AUTO_INCREMENT değeri `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
