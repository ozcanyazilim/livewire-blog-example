<?php

use App\Models\Blog;
use App\Models\Category;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/home', function () {
    return view('welcome');
});
Route::get('/', function () {
    return view('welcome');
});


Route::get('/kategori/{category_slug}', function ($category_slug) {
    $category = Category::where('slug', $category_slug)->first();
    return view('kategoriye_gore_goster', ['cat' => $category]);
});


Route::get('/detay/{slug}', function ($slug) {
    $content = Blog::where('slug', $slug)->firstOrFail();
    return view('detay_goster', ['content' => $content]);
});


Route::get('/yazar/{slug}', function ($slug) {
    $author = User::where('name', $slug)->first();
    return view('yazara_gore_goster', ['author' => $author]);
});


Route::group(['middleware' => 'guest'], function () {


    Route::post('/giris-yap', function (Request $request) {
        $user = User::where('email', $request->get('mail_adresi'))->firstOrFail();
        if (Auth::attempt(array('email' => $user->email, 'password' => $request->get('parola')))) {
            $request->session()->regenerate();
            return redirect()->to('/')->with('success', 'Giriş Başarıyla Yapıldı!');
        } else {
            return redirect()->to('/')->with('success', 'Kullanıcı adı veya Parola Hatalı!');
        }
    });

    Route::get('/kayit-ol', function () {
        return view('kayit_ol');
    });

    Route::post('/kayit-ol', function (Request $request) {
        $userControl = User::where('email', $request->get('mail_adresi'))->first();
        if ($userControl != null) {
            return redirect()->to('/kayit-ol')->with('error', 'Bu Kullanıcı daha önceden kayıt olmuş!');
        }

        $user = new User();
        $user->name = Str::slug($request->get('name'), '_');
        $user->email = $request->get('mail_adresi');
        $user->password = Hash::make($request->get('parola'));
        $user->save();
        return redirect()->to('/')->with('success', 'Başarıyla Kayıt Olundu');
    });
});


Route::group(['middleware' => 'auth'], function () {
    Route::get('/cikis-yap', function () {
        Auth::logout();
        return redirect()->to('/')->with('success', 'Çıkış Başarıyla Yapıldı!');
    });
});
