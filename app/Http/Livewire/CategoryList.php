<?php

namespace App\Http\Livewire;

use App\Models\Category;
use Livewire\Component;

class CategoryList extends Component
{
    public $category_id;

    public function render()
    {
        return view('livewire.category-list', ['cat_id' => $this->category_id, 'categories' => Category::orderBy('title', 'asc')->get()]);
    }
}
