<?php

namespace App\Http\Livewire;

use App\Models\Category;
use Livewire\Component;
use App\Models\Blog;
use Illuminate\Support\Facades\Auth;
use Livewire\WithFileUploads;
use Illuminate\Support\Str;

class CreatePost extends Component
{

    use WithFileUploads;


    public $title;
    public $content;
    public $category_id = "";
    public $photo;


    public function addPost()
    {
        if (Auth::check()) {
            $this->validate([
                'title' => 'required',
                'content' => 'required',
                'category_id' => 'required'
            ], [
                'title.required' => 'Başlık Alanını Boş Bırakmayın!',
                'content.required' => 'İçerik Alanını Boş Bırakmayın!',
                'category_id.required' => 'Lütfen Bir Kategori Seçin!',
            ]);

            if ($this->photo != null && $this->photo != '') {
                $dosya_url = Str::slug($this->title) . '-' . time() . '.' . $this->photo->extension();

                //$lokasyon = public_path('photos/' . $dosya_url);
                $this->photo->storeAs('public/photos', $dosya_url);
                $this->photo = $dosya_url;
            } else {
                $this->photo = "0";
            }
            $post = new Blog();
            $post->title = $this->title;
            $post->slug = Str::slug($this->title);
            $post->content = $this->content;
            $post->category_id = $this->category_id;
            $post->image = $this->photo;
            $post->vote = 0;
            $post->user_id = Auth::user()->id;
            $post->save();

            session()->flash('message', 'İçerik Başarıyla Oluşturuldu!');
            $this->emit('refreshData');
            $this->title = "";
            $this->content = "";
            $this->category_id = "";
            $this->photo = null;
        } else {
            session()->flash('message', 'Lütfen Giriş Yapın!');
        }
    }


    public function render()
    {
        return view('livewire.create-post', ['categories' => Category::orderBy('title', 'asc')->get()]);
    }
}
