<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Blog;
use Livewire\WithPagination;

class ShowPosts extends Component
{
    private $posts;
    public $search = '';
    public $category_id = 0;
    public $orderBy = ' ';
    public $author_id = 0;

    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    protected $listeners = [
        'refreshData'
    ];

    public function refreshData()
    {
        // $this->posts = Blog::orderBy('created_at', 'desc')->paginate(3);
        $this->resetPage();
    }

    public function updatingSearch()
    {

        $this->gotoPage(1);
        // $this->resetPage();
    }

    public function render()
    {
        $this->posts = Blog::where('title', 'like', '%' . $this->search . '%');
        if ($this->category_id != 0)
            $this->posts = $this->posts->where('title', 'like', '%' . $this->search . '%')->where('category_id', $this->category_id);

        if ($this->author_id != 0)
            $this->posts = $this->posts->where('title', 'like', '%' . $this->search . '%')->where('user_id', $this->author_id);

        switch ($this->orderBy) {
            case 'a-z':
                $this->posts = $this->posts->orderBy('title', 'asc');
                break;
            case 'z-a':
                $this->posts = $this->posts->orderBy('title', 'desc');
                break;
            case 'yeni':
                $this->posts = $this->posts->orderBy('created_at', 'desc');
                break;
            case 'eski':
                $this->posts = $this->posts->orderBy('created_at', 'asc');
                break;
        }
        $this->posts = $this->posts->paginate(5);
        return view('livewire.show-posts', ['posts' => $this->posts]);
    }
}
