<div class="col-3">
    <div class="col-12">
        @if(Auth::user() && Auth::user()->id > 0)
            <div class="border p-3 mb-3 shadow rounded text-center">
                {{'@'.Auth::user()->name.' - '.Auth::user()->email}}
                <hr>
                <a href="/cikis-yap" class="btn-link">Çıkış Yap</a>
            </div>
        @else
            <form action="/giris-yap" method="post" class="border p-3 mb-3 shadow rounded">
                @csrf
                <div class="form-group">
                    <label for="">Mail Adresi</label>
                    <input type="text" name="mail_adresi" class="form-control">
                </div>
                <div class="form-group">
                    <label for="">Parola</label>
                    <input type="password" name="parola" class="form-control">
                </div>
                <div class="form-group">
                    <button class="btn btn-block btn-primary">Giriş Yap</button>
                    <hr>
                    <center>
                        <a href="/kayit-ol" class="btn-link">Kayıt Ol</a>
                    </center>
                </div>
            </form>
        @endif
        @livewire('category-list', ['category_id' => $category_id])
    </div>
</div>
