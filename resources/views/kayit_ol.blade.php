<!doctype html>
<html lang="en">
  <head>
    <title>Liveware Blog App</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.1/css/all.css" integrity="sha384-O8whS3fhG2OnA5Kas0Y9l3cfpmYjapjI0E4theH4iuMD+pLhbf6JI0jIMfYcK3yZ" crossorigin="anonymous">
    <style>
        .createForm{
            display:none;
        }
    </style>

    <script src="https://code.jquery.com/jquery-3.3.1.min.js" ></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

    <script>
        $(document).ready(function(){
            $('.text-sm').fadeIn().delay(10000).fadeOut();
            $(document).delegate('.createFormToggle', 'click', function(){
                $('.createForm').toggle(300);
            });
        });
    </script>


    @livewireStyles
    @livewireScripts

    </head>
  <body>

    <div class="container-fluid" style="margin:6rem auto !important;">
        <div class="row">
            @include('sol_taraf', ['category_id' => 0])
            <div class="col-9">
                <form action="" method="post">
                    @csrf
                    <div class="form-group">
                        <label for="">Kullanıcı Adınız</label>
                        <input type="text" name="name" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="">Mail Adresiniz</label>
                        <input type="email" name="mail_adresi"  class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="">Parolanız</label>
                        <input type="password" name="parola"  class="form-control">
                    </div>
                    <div class="form-group">
                        <button class="btn btn-block btn-primary">
                            Kayıt Ol
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>



</body>
</html>
