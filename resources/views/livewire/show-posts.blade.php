<div class="col-12" style="margin-top:4rem !important;">
    <div class="row">
            <div class="col-4">
        <div class="form-group">
            <label for="">Sırala</label>
            <select wire:model="orderBy" id="" class="form-control">
                <option value=" " selected disabled></option>
                <option value="a-z">Alfabetik A > Z</option>
                <option value="z-a">Alfabetik Z > A</option>
                <option value="yeni">Önce En Yeni</option>
                <option value="eski">Önce En Eski</option>
            </select>
        </div>
    </div>
    <div class="col-4 offset-4">
        <div class="form-group">
            <label for="">Aramak istediğiniz kelimeyi giriniz</label>
            <input type="text" wire:model="search" class="form-control">
        </div>
    </div>
    </div>
    @if($posts->total() > 0)
    @foreach($posts as $post)
    <div class="col-12 p-4 border text-white shadow" style="background-color:#3d4752!important;">
        <h3 class="text-center">
            {{$post->title}}
        </h3>
        @if($post->image != "0")
            <img src="{{Storage::url('photos/' . $post->image)}}" alt="" class="img-fluid">
        @endif
        <p>
            {{substr($post->content,0, 200). '...'}} <a href="/detay/{{$post->slug}}" class="font-weight-bold text-light"> Devamını Oku</a>
        </p>
        <hr class="bg-light">
        <div class="row h-100">
            <div class="col-6">
                <i class="fas fa-calendar"></i> Tarih : {{$post->created_at->diffForHumans()}}
                <br>
                <a href="/yazar/{{$post->user->name}}" class="font-weight-bold text-light"> <i class="fas fa-user"></i> Yazar : {{$post->user->name }}</a>
                <br>
                <a href="/kategori/{{$post->category->slug}}" class="text-light font-weight-bolder"><i class="fas fa-file"></i> Kategori : {{$post->category->title }}</a>
            </div>
            <div class="col-6 text-right align-middle">
                <a href="/detay/{{$post->slug}}" class="font-weight-bold text-light"> Devamını Oku</a>
            </div>
        </div>

    </div>
    @endforeach
    <div class="col-12">
        <hr>
            {{ $posts->links() }}
    </div>
    @else
        <div class="alert alert-danger">
            <strong>Herhangi bir gönderi bulunamadı!</strong>
        </div>
    @endif
</div>
