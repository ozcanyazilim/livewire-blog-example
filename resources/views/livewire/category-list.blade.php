<div class="list-group">
    <a href="/" class="list-group-item list-group-item-action bg-secondary text-light">
        Anasayfa
    </a>
    @foreach($categories as $category)
        <a href="/kategori/{{$category->slug}}" class="list-group-item list-group-item-action {{$category->category_id == $cat_id ? 'active' : null}}">
            {{$category->title}}
        </a>
    @endforeach
</div>
