<div class="col-12">
    <form wire:submit.prevent="addPost" class="shadow-lg p-3">

        @if(session()->has('message'))
            <div class="alert alert-info">
                <strong>{{session('message')}}</strong>
            </div>
        @endif

        <div class="form-group">
            <label for="">Başlık</label>
            <input type="text" wire:model="title" class="form-control">
            @error('title')
                <label for="" class="text-danger text-sm">{{$message}}</label>
            @enderror
        </div>
        <div class="form-group">
            <label for="">İçerik</label>
            <textarea name="" class="form-control" wire:model="content" id="" cols="30" rows="4"></textarea>
            @error('content')
                <label for="" class="text-danger text-sm">{{$message}}</label>
            @enderror
        </div>

        <div class="form-group">
            <label for="">Kategori</label>
            <select  wire:model="category_id" id="" class="form-control">
                <option value="" disabled selected >Lütfen Seçiniz</option>
                @foreach($categories as $category)
                    <option value="{{$category->category_id}}">{{$category->title}}</option>
                @endforeach
            </select>
            @error('category_id')
                <label for="" class="text-danger text-sm">{{$message}}</label>
            @enderror
        </div>

        <div class="form-group">
            <label for="">İçerik Görseli</label>
            <input type="file" wire:model="photo">
        </div>

        <div class="form-group">
            <button class="btn btn-success float-right" type="submit">Kaydet</button>
            <div class="clearfix"></div>
        </div>
    </form>
</div>
