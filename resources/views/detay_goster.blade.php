<!doctype html>
<html lang="en">
  <head>
    <title>Liveware Blog App</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.1/css/all.css" integrity="sha384-O8whS3fhG2OnA5Kas0Y9l3cfpmYjapjI0E4theH4iuMD+pLhbf6JI0jIMfYcK3yZ" crossorigin="anonymous">
    <style>
        .createForm{
            display:none;
        }
    </style>

    <script src="https://code.jquery.com/jquery-3.3.1.min.js" ></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

    <script>
        $(document).ready(function(){
            $('.text-sm').fadeIn().delay(10000).fadeOut();
            $(document).delegate('.createFormToggle', 'click', function(){
                $('.createForm').toggle(300);
            });
        });
    </script>


    @livewireStyles
    @livewireScripts

    </head>
  <body>

     <div class="container-fluid" style="margin:6rem auto !important;">
        <div class="row">
            @include('sol_taraf', ['category_id' => $content->category_id])
            <div class="col-9">
                {{-- @livewire('create-post') --}}
                <div class="col-12">
                    <h3>{{$content->title}}</h3>
                    <hr>
                    @if($content->image != null)
                        <img src="{{Storage::url('photos/' . $content->image)}}" alt="" class="img-fluid">
                    @endif
                    <p class="mt-3">
                        <label for=""><i class="fas fa-user"></i> Yazar : <a href="/yazar/{{$content->user->name}}">{{'@'.$content->user->name}}</a></label>
                        <label for="" class="pl-2"><i class="fas fa-calendar"></i> Tarih : {{$content->created_at->diffForHumans()}}</label>
                        <label for="" class="pl-2"><i class="fas fa-file"></i> Kategori : <a href="/kategori/{{$content->category->slug}}">{{$content->category->title}}</a></label>
                    </p>
                    <p>{{$content->content}}</p>
                </div>
            </div>
        </div>
    </div>



</body>
</html>
